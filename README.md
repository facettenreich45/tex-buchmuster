# tex-buchmuster

Mit LaTeX umfangreiche Texte in hoher Ausgabequalität zu erstellen ist aufwendig.

Mit einem Muster wird der Einstieg in das Thema und die Anfertigung erleichtert. 

Vorgestellt wird mein Musterprojekt für ein Buch, mit dem ich mehrere Bücher veröffentlicht habe. 

Das Muster ist auf Biber und Glossaries und auch sonst soweit erforderlich aktualisiet [Stand: 20230608].

In der .pdf sind ergänzende Hinweise aufgeführt.



